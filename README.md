# create-angular

## Build docker image and push it to the dockerhub

    docker build -t neatsoft/create-angular .
    docker login
    docker push neatsoft/create-angular

## Create new project

    docker run -it -u `id -u`:`id -g` -v `pwd`:/app neatsoft/create-angular <project_name>
