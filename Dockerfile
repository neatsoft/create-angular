FROM node:10-slim

WORKDIR /app

RUN npm install -g @angular/cli

COPY ./entrypoint /

ENTRYPOINT ["/entrypoint"]
